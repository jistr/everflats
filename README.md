Everflats
=========

Wannabe 2D game, currently 2D game engine demo. Server written in
Rust, web client written in Rust WASM and JavaScript.

* [Developer docs](docs/devel/readme.md)
