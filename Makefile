.PHONY: build clean test test-unit
.DEFAULT_GOAL := build
SHELL := /bin/bash
ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

COMMON_DIR := $(ROOT_DIR)/common
SERVER_DIR := $(ROOT_DIR)/server
WUI_DIR := $(ROOT_DIR)/wui


# BUILD

NPX_WEBPACK_MODE ?= production
CARGO_BUILD_FLAGS ?= --release
TARGET_SUBDIR ?= release

build: build-server build-wui

build-server:
	cd $(SERVER_DIR) && \
	cargo build $(CARGO_BUILD_FLAGS)

build-wui:
	cd $(WUI_DIR) && \
	rm -r dist/* || true && \
	cargo build $(CARGO_BUILD_FLAGS) --target wasm32-unknown-unknown --bin everflats_wasm_app && \
	cargo build $(CARGO_BUILD_FLAGS) --target wasm32-unknown-unknown --bin everflats_wasm_game_worker && \
	cd $(ROOT_DIR) && \
	wasm-bindgen --target web --no-typescript --out-dir wui/dist --out-name app target/wasm32-unknown-unknown/$(TARGET_SUBDIR)/everflats_wasm_app.wasm && \
	wasm-bindgen --target no-modules --no-typescript --out-dir wui/dist --out-name game_worker target/wasm32-unknown-unknown/$(TARGET_SUBDIR)/everflats_wasm_game_worker.wasm && \
	cd $(WUI_DIR) && \
	npx webpack --mode $(NPX_WEBPACK_MODE)


# CLEAN

clean: clean-common clean-server clean-wui

clean-common:
	cd $(COMMON_DIR) && cargo clean

clean-server:
	cd $(SERVER_DIR) && cargo clean

clean-wui:
	cd $(WUI_DIR) && rm -rf dist && rm -rf node_modules && cargo clean


# DOC

doc:
	cargo doc --workspace


# PREREQ

prereq: prereq-wui

prereq-wui:
	cargo install wasm-bindgen-cli; \
	cd $(WUI_DIR) && npm install


# TEST

test: test-unit
	echo "ALL TESTS OK"

test-unit: test-common-unit test-server-unit test-wasm-unit
	echo "UNIT TESTS OK"

test-common-unit:
	cd $(COMMON_DIR) && cargo test

test-server-unit:
	cd $(SERVER_DIR) && cargo test

test-wasm-unit:
	cd $(WUI_DIR) && cargo test
