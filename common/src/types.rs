use std::collections::HashMap;

pub use crate::result::{Result, Error, ErrorKind};

pub type ClientId = u64;
pub type Coord = i32;
pub type EntitiesMap = HashMap<EntityId, Entity>;
pub type EntityId = u32;
pub type EntityT = u32;
pub type LocationEntitiesMap = HashMap<Location, Vec<Entity>>;
pub type MsgT = u16;

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Entity {
    pub id: EntityId,
    pub entity_type: EntityT,
    pub location: Location,
}

#[derive(Copy, Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct Location {
    pub x: Coord,
    pub y: Coord,
}

#[derive(Copy, Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub enum Direction {
    N,
    S,
    E,
    W,
}
