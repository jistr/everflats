pub mod entity_t;

// 12 to all sides + the tile in the center => box of 25x25
pub const VISION_RANGE: i32 = 12;
