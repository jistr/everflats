#![allow(dead_code)]

use everflats_macro::const_list;
use crate::types::*;

const_list!((EntityT, [
    // TERRAIN - 0x1000
    (T_DIRT, 0x1000_0001),
    (T_STONE, 0x1000_0010),

    // PLANTS - 0x3000
    (P_GRASS_SHORT, 0x3000_0001),
    (P_GRASS_TALL, 0x3000_0002),

    // ITEMS - 0x5000
    (I_STONE, 0x5000_0010),

    // ACTORS - 0x7000
    (A_HUMAN, 0x7000_0001),

    // GASSES - 0x9000

    // FADING / EFFECTS - 0xa000
    (F_LIGHTNING, 0xa000_1000),

    // SPECIAL - 0xc000
    (S_ADMIN_VIEW, 0xc000_0001),
]));
