use crate::types::*;
use bincode;

#[derive(Serialize, Deserialize, Debug)]
pub enum Msg {
    // Server -> Client
    VisionCenter(Location),
    Entities(Vec<Entity>),
    BatchEnd,

    // Client -> Server
    Walk(Direction),
}

impl Msg {
    pub fn from_bincode(bincode: &[u8]) -> Result<Self> {
        Ok(bincode::deserialize(bincode)?)
    }

    pub fn to_bincode(&self) -> Result<Vec<u8>> {
        Ok(bincode::serialize(self)?)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_serialize_deserialize() {
        let msg = Msg::Entities(vec![]);
        let bytes: Vec<u8> = msg.to_bincode().expect("Failed to serialize");
        let msg_deser: Msg = Msg::from_bincode(&bytes).expect("Failed to deserialize");
        if let Msg::Entities(_) = msg_deser {
        } else {
            panic!("Incorrectly deserialized")
        }
    }
}
