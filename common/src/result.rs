#[cfg(feature = "crossbeam-channel")]
use crossbeam_channel::{TryRecvError, TrySendError};
#[cfg(feature = "wasm-bindgen")]
use wasm_bindgen::prelude::JsValue;

use std::error::Error as StdError;

pub type Result<T> = ::std::result::Result<T, Error>;

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    message: String,
    source: Option<Box<dyn StdError + Send + Sync + 'static>>,
}

impl Error {
    pub fn new<S: Into<String>>(kind: ErrorKind, message: S) -> Self {
        Self {
            kind: kind,
            message: message.into(),
            source: None,
        }
    }

    pub fn network<S: Into<String>>(message: S) -> Self {
        Self::new(ErrorKind::Network, message)
    }

    pub fn render<S: Into<String>>(message: S) -> Self {
        Self::new(ErrorKind::Render, message)
    }

    pub fn with_source(mut self, source: Box<dyn StdError + Send + Sync + 'static>) -> Self {
        self.source = Some(source);
        self
    }

    fn source_fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        match &self.source {
            Some(err) => write!(f, "({})", err),
            None => Ok(()),
        }
    }
}

impl ::std::error::Error for Error {
    fn source(&self) -> Option<&'static dyn (::std::error::Error)> {
        match &self.kind {
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum ErrorKind {
    Bincode,
    #[cfg(feature = "crossbeam-channel")]
    Channel,
    #[cfg(feature = "wasm-bindgen")]
    JavaScript,
    Network,
    Render,
    __Nonexhaustive,
}

impl ErrorKind {
    pub fn name(&self) -> &'static str {
        match self {
            ErrorKind::Bincode => "Bincode",
            #[cfg(feature = "crossbeam-channel")]
            ErrorKind::Channel => "Channel",
            #[cfg(feature = "wasm-bindgen")]
            ErrorKind::JavaScript => "JavaScript",
            ErrorKind::Network => "Network",
            ErrorKind::Render => "Render",
            _ => unreachable!(),
        }
    }
}

impl From<bincode::Error> for Error {
    fn from(error: bincode::Error) -> Self {
        Self::new(ErrorKind::Bincode, "").with_source(Box::new(error))
    }
}

#[cfg(feature = "crossbeam-channel")]
impl From<TryRecvError> for Error {
    fn from(error: TryRecvError) -> Self {
        Self::new(ErrorKind::Channel, "").with_source(Box::new(error))
    }
}

#[cfg(feature = "crossbeam-channel")]
impl<T: 'static + Send + Sync> From<TrySendError<T>> for Error {
    fn from(error: TrySendError<T>) -> Self {
        Self::new(ErrorKind::Channel, "").with_source(Box::new(error))
    }
}

#[cfg(feature = "wasm-bindgen")]
impl From<JsValue> for Error {
    fn from(error: JsValue) -> Self {
        Self::new(ErrorKind::JavaScript, &format!("{:?}", error))
    }
}

impl ::std::fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self.name())
    }
}

impl ::std::fmt::Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}: {}", self.kind.name(), self.message)?;
        self.source_fmt(f)
    }
}
