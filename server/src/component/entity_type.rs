use specs::{Component, VecStorage};

use common::types::*;

pub struct EntityType {
    pub et: EntityT,
}

impl Component for EntityType {
    type Storage = VecStorage<Self>;
}
