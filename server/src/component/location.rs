use specs::{Component, VecStorage};

use common::types::{Coord, Location as ProtoLocation};

#[derive(Clone, Debug)]
pub struct Location {
    pub x: Coord,
    pub y: Coord,
}

impl Component for Location {
    type Storage = VecStorage<Self>;
}

impl Into<ProtoLocation> for &Location {
    fn into(self) -> ProtoLocation {
        ProtoLocation {
            x: self.x,
            y: self.y,
        }
    }
}
