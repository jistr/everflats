use specs::{Component, Entity, DenseVecStorage};

use common::types::*;


pub struct Client {
    pub client_id: ClientId,
}

impl Component for Client {
    type Storage = DenseVecStorage<Self>;
}


pub struct ClientControl {
    pub entity: Entity,
}

impl Component for ClientControl {
    type Storage = DenseVecStorage<Self>;
}


pub struct ClientView {
    pub entity: Entity,
}

impl Component for ClientView {
    type Storage = DenseVecStorage<Self>;
}
