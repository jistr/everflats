use specs::{Entity, ReadStorage, WriteStorage};

use common::consts::entity_t;
use common::types::Direction;
use crate::component::*;

pub struct WalkAction {
    pub entity: Entity,
    pub direction: Direction,
}

impl WalkAction {
    pub fn process(&self, locs: &mut WriteStorage<'_, Location>,
                   ets: &ReadStorage<'_, EntityType>) {
        let et_opt = ets.get(self.entity);
        if et_opt.is_none() {
            return;  // Only entities with EntityType can walk.
        }
        let et = et_opt.expect("Failed to unwrap EntityType despite checking for None");
        match et.et {
            entity_t::S_ADMIN_VIEW => self.execute_move(locs),  // Admin view can go anywhere.
            _ => (),  // Nothing else can walk.
        }
    }

    fn execute_move(&self, locs: &mut WriteStorage<'_, Location>) {
        if let Some(loc) = locs.get_mut(self.entity) {  // Only entities with Location can walk.
            match self.direction {
                Direction::N => loc.y += 1,
                Direction::S => loc.y -= 1,
                Direction::E => loc.x += 1,
                Direction::W => loc.x -= 1,
            }
        }
    }
}
