mod walk;

pub use self::walk::WalkAction;

pub enum Action {
    Walk(WalkAction),
}
