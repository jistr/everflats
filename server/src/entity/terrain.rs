use specs::{Builder, Entity, EntityBuilder};

use common::types::*;
use component::{Location, EntityType};

pub fn build_terrain(builder: EntityBuilder, x: Coord, y: Coord, et: EntityT) -> Entity {
    builder
        .with(Location {x: x, y: y})
        .with(EntityType {et: et})
        .build()
}
