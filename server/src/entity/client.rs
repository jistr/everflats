use specs::{Builder, Entities, Entity, EntityBuilder, Join, ReadStorage, WriteStorage,
            world::EntityResBuilder};

use common::consts::entity_t;
use common::types::ClientId;
use component::*;

pub fn build_client_res(builder: EntityResBuilder, client_id: ClientId,
                        clients: &mut WriteStorage<'_, Client>) -> Entity {
    builder
        .with(Client {client_id: client_id}, clients)
        .build()
}

pub fn delete_client_res(client_id: ClientId, entities: &mut Entities<'_>,
                         clients: &mut WriteStorage<'_, Client>) {
    // delete the Client entity
    let mut to_delete: Vec<Entity> = vec![];
    for (entity, client) in (&*entities, &*clients).join() {
        if client.client_id == client_id {
            to_delete.push(entity);
        }
    }
    for entity in to_delete.into_iter() {
        entities.delete(entity)
            .expect("Fatal error: attempted to delete Client entity of wrong generation");
    }
}

pub fn build_admin_view(builder: EntityBuilder) -> Entity {
    builder
        .with(EntityType { et: entity_t::S_ADMIN_VIEW })
        .with(Location { x: 0, y: 0 })
        .build()
}

pub fn fetch_admin_view_res(entities: &Entities<'_>, entity_types: &ReadStorage<'_, EntityType>)
                            -> Option<Entity> {
    for (entity, entity_type) in (entities, entity_types).join() {
        if entity_type.et == entity_t::S_ADMIN_VIEW {
            return Some(entity);
        }
    }
    return None
}

pub fn attach_view_to_entity_res(client_entity: Entity, view_entity: Entity,
                                 entity_views: &mut WriteStorage<'_, ClientView>) {
    debug!("Attached view for client entity '{:?}' to entity '{:?}'.",
           client_entity, view_entity);
    entity_views.insert(client_entity, ClientView { entity: view_entity })
        .expect("Failed to attach client view to entity");
}

pub fn attach_view_to_admin_view_res(client_entity: Entity, entities: &Entities<'_>,
                                     entity_types: &ReadStorage<'_, EntityType>,
                                     entity_views: &mut WriteStorage<'_, ClientView>) {
    let admin_view = fetch_admin_view_res(entities, entity_types)
        .expect("Fatal: S_ADMIN_VIEW entity not found");
    attach_view_to_entity_res(client_entity, admin_view, entity_views);
}

pub fn attach_control_to_entity_res(client_entity: Entity, control_entity: Entity,
                                    entity_controls: &mut WriteStorage<'_, ClientControl>) {
    debug!("Attached control for client entity '{:?}' to entity '{:?}'.",
           client_entity, control_entity);
    entity_controls.insert(client_entity, ClientControl { entity: control_entity })
        .expect("Failed to attach client control to entity");
}

pub fn attach_control_to_admin_view_res(client_entity: Entity, entities: &Entities<'_>,
                                        entity_types: &ReadStorage<'_, EntityType>,
                                        entity_controls: &mut WriteStorage<'_, ClientControl>) {
    let admin_view = fetch_admin_view_res(entities, entity_types)
        .expect("Fatal: S_ADMIN_VIEW entity not found");
    attach_control_to_entity_res(client_entity, admin_view, entity_controls);
}
