use crossbeam_channel::{Sender, Receiver, TryRecvError};
use specs::{Entities, ReadStorage, System};
use specs::join::Join;
use std::collections::{HashMap, HashSet};

use common::consts::VISION_RANGE;
use common::protocol::Msg;
use common::types::*;
use component::{Client, ClientView, EntityType, Location as SLocation};
use net::socket::ClientSenderRegistration;

pub struct NetOutputSystem {
    registrar: Receiver<ClientSenderRegistration>,
    client_ids: HashSet<ClientId>,
    caches: HashMap<ClientId, ClientOutputCache>,
    senders: HashMap<ClientId, Sender<Msg>>,
}

impl NetOutputSystem {
    pub fn new(registrar: Receiver<ClientSenderRegistration>) -> Self {
        NetOutputSystem {
            registrar: registrar,
            client_ids: HashSet::new(),
            caches: HashMap::new(),
            senders: HashMap::new(),
        }
    }

    fn register_and_unregister_clients(&mut self) {
        loop {
            match self.registrar.try_recv() {
                Ok(reg_msg) => {
                    match reg_msg {
                        ClientSenderRegistration::Register(id, receiver) =>
                            self.register_client(id, receiver),
                        ClientSenderRegistration::Unregister(id) =>
                            self.unregister_client(id),
                    }
                },
                Err(TryRecvError::Empty) => return,
                Err(TryRecvError::Disconnected) => panic!("NetOutputSystem registrar disconnected"),
            }
        }
    }

    fn register_client(&mut self, id: ClientId, sender: Sender<Msg>) {
        info!("Registering output for client with id {}", id);

        assert!(self.client_ids.insert(id),
                "Registered client output with ID which was already present");
        self.senders.insert(id, sender);
        self.caches.insert(id, ClientOutputCache::new());
    }

    fn unregister_client(&mut self, id: ClientId) {
        info!("Unregistering output for client with id {}", id);
        self.client_ids.remove(&id);
        self.senders.remove(&id);
        self.caches.remove(&id);
    }

    fn update_vision_cache(&mut self, data: &<Self as System>::SystemData) {
        for cache in self.caches.values_mut() {
            cache.vision_center_previous = cache.vision_center.take();
            cache.entity_view = None;
        }

        let (_entities, clients, views, _ets, locs) = data;
        for (client, view) in (clients, views).join() {
            if !self.client_ids.contains(&client.client_id) {
                debug!("Client id {} is not registered in output system, \
                        not processing vision cache for this client.", &client.client_id);
                continue;
            }

            let cache = self.caches.get_mut(&client.client_id)
                .expect(&format!("Failed to get output cache for client {}", client.client_id));
            cache.entity_view = Some(view.entity.id());
            match locs.get(view.entity) {
                Some(loc) => cache.vision_center = Some((*loc).clone()),
                None => warn!("Client {} attached to entity {:?} which has no location.",
                              client.client_id, view.entity),
            }
        }
    }

    fn send_state_to_clients(&self, data: &<Self as System>::SystemData) {
        for id in self.client_ids.iter() {
            Self::send_state(
                *id,
                self.caches.get(id)
                    .expect(&format!("NetOutputSystem cache not found for client id {}", id)),
                self.senders.get(id)
                    .expect(&format!("NetOutputSystem sender not found for client id {}", id)),
                data)
        }
    }

    fn send_state(client_id: ClientId, cache: &ClientOutputCache, sender: &Sender<Msg>,
                  data: &<Self as System>::SystemData) {
        Self::send_entities(client_id, cache, sender, data);
        Self::send_vision_center(client_id, cache, sender);
        Self::send_batch_end(client_id, sender);
    }

    fn send_entities(client_id: ClientId, cache: &ClientOutputCache, sender: &Sender<Msg>,
                     data: &<Self as System>::SystemData) {
        if cache.vision_center.is_none() {
            return;
        }
        let vision_center = cache.vision_center.as_ref().unwrap();

        let mut new_entities: Vec<Entity> = Vec::new();
        let (_entities, _clients, _views, ets, locs) = data;
        // FIXME: Is there a more efficient way than iterating over everything?
        for (entity, et, loc) in (_entities, ets, locs).join() {
            let e_x = &loc.x;
            let e_y = &loc.y;
            let c_x = &vision_center.x;
            let c_y = &vision_center.y;
            if *e_x <= c_x + VISION_RANGE &&
               *e_x >= c_x - VISION_RANGE &&
               *e_y <= c_y + VISION_RANGE &&
               *e_y >= c_y - VISION_RANGE {
                   new_entities.push(Entity { id: entity.id(),
                                              entity_type: et.et,
                                              location: loc.into()})
            }
        }

        trace!("Sending Entities to client {}", client_id);
        sender.send(Msg::Entities(new_entities)).expect("NetOutputSystem failed to send Entities");
    }

    fn send_vision_center(client_id: ClientId, cache: &ClientOutputCache, sender: &Sender<Msg>) {
        if let Some(ref vc) = cache.vision_center {
            trace!("Sending VisionCenter to client {}", client_id);
            sender.send(Msg::VisionCenter(Location { x: vc.x, y: vc.y }))
                .expect("NetOutputSystem failed to send VisionCenter");
        }
    }

    fn send_batch_end(client_id: ClientId, sender: &Sender<Msg>) {
        trace!("Sending BatchEnd to client {}", client_id);
        sender.send(Msg::BatchEnd).expect("NetOutputSystem failed to send BatchEnd");
    }
}

impl<'a> System<'a> for NetOutputSystem {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Client>,
        ReadStorage<'a, ClientView>,
        ReadStorage<'a, EntityType>,
        ReadStorage<'a, SLocation>,
    );

    fn run(&mut self, data: Self::SystemData) {
        self.register_and_unregister_clients();
        self.update_vision_cache(&data);
        self.send_state_to_clients(&data)
    }
}


struct ClientOutputCache {
    entity_view: Option<EntityId>,
    vision_center: Option<SLocation>,
    vision_center_previous: Option<SLocation>,
}

impl ClientOutputCache {
    pub fn new() -> Self {
        ClientOutputCache {
            entity_view: None,
            vision_center: None,
            vision_center_previous: None,
        }
    }
}
