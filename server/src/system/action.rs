use crossbeam_channel::Receiver;
use specs::{ReadStorage, System, WriteStorage};

use crate::action::*;
use crate::component::*;

pub struct ActionSystem {
    action_recv: Receiver<Action>,
}

impl ActionSystem {
    pub fn new(action_recv: Receiver<Action>) -> Self {
        ActionSystem {
            action_recv: action_recv,
        }
    }

    fn process_actions(&mut self, data: &mut <Self as System>::SystemData) {
        let (ref mut locs, ets) = data;
        while let Ok(action) = self.action_recv.try_recv() {
            match action {
                Action::Walk(walk) => walk.process(locs, ets),
            }
        }
    }
}

impl<'a> System<'a> for ActionSystem {
    type SystemData = (
        WriteStorage<'a, Location>,
        ReadStorage<'a, EntityType>,
    );

    fn run(&mut self, mut data: Self::SystemData) {
        self.process_actions(&mut data);
    }
}
