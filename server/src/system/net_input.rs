use crossbeam_channel::{Receiver, Sender, TryRecvError};
use specs::{Entity, Entities, Join, ReadStorage, System, WriteStorage};
use std::collections::{HashMap, HashSet};

use common::protocol::Msg;
use common::types::ClientId;
use crate::action::*;
use crate::component::*;
use crate::entity::client as ent_client;
use crate::net::socket::ClientReceiverRegistration;

pub struct NetInputSystem {
    action_s: Sender<Action>,
    caches: HashMap<ClientId, ClientInputCache>,
    client_ids: HashSet<ClientId>,
    registrar: Receiver<ClientReceiverRegistration>,
    receivers: HashMap<ClientId, Receiver<Msg>>,
}

impl NetInputSystem {
    pub fn new(action_s: Sender<Action>, registrar: Receiver<ClientReceiverRegistration>) -> Self {
        NetInputSystem {
            action_s: action_s,
            client_ids: HashSet::new(),
            caches: HashMap::new(),
            registrar: registrar,
            receivers: HashMap::new(),
        }
    }

    fn register_and_unregister_clients(&mut self, data: &mut <Self as System>::SystemData) {
        loop {
            match self.registrar.try_recv() {
                Ok(reg_msg) => {
                    match reg_msg {
                        ClientReceiverRegistration::Register(id, receiver) =>
                            self.register_client(id, receiver, data),
                        ClientReceiverRegistration::Unregister(id) =>
                            self.unregister_client(id, data),
                    }
                },
                Err(TryRecvError::Empty) => return,
                Err(TryRecvError::Disconnected) => panic!("NetInputSystem registrar disconnected"),
            }
        }
    }

    fn register_client(&mut self, id: ClientId, receiver: Receiver<Msg>,
                       data: &mut <Self as System>::SystemData) {
        info!("Registering input from client with id {}", id);
        assert!(self.client_ids.insert(id),
                "Registered client output with ID which was already present");
        self.receivers.insert(id, receiver);
        self.caches.insert(id, ClientInputCache::new());
        let (ref mut entities, ref mut clients, ref mut controls, ref mut views, ref entity_types)
            = data;
        let client = ent_client::build_client_res(entities.build_entity(), id, clients);
        ent_client::attach_view_to_admin_view_res(client, entities, entity_types, views);
        ent_client::attach_control_to_admin_view_res(client, entities, entity_types, controls);
    }

    fn unregister_client(&mut self, id: ClientId, data: &mut <Self as System>::SystemData) {
        info!("Unregistering input from client with id {}", id);
        self.client_ids.remove(&id);
        self.receivers.remove(&id);
        self.caches.remove(&id);
        let (ref mut entities, ref mut clients, _, _, _) = data;
        ent_client::delete_client_res(id, entities, clients);
    }

    fn update_input_cache(&mut self, data: &<Self as System>::SystemData) {
        for cache in self.caches.values_mut() {
            cache.entity_control = None;
        }

        let (_, clients, controls, _, _) = data;
        for (client, control) in (clients, controls).join() {
            if !self.client_ids.contains(&client.client_id) {
                debug!("Client id {} is not registered in input system, \
                        not processing input cache for this client.", &client.client_id);
                continue;
            }

            let cache = self.caches.get_mut(&client.client_id)
                .expect(&format!("Failed to get input cache for client {}", client.client_id));
            cache.entity_control = Some(control.entity);
        }
    }

    fn process_msgs(&mut self, data: &mut <Self as System>::SystemData) {
        for (id, receiver) in self.receivers.iter() {
            while let Ok(msg) = receiver.try_recv() {
                self.process_msg(*id, msg, data);
            }
        }
    }

    fn process_msg(&self, client_id: ClientId, msg: Msg, _data: &mut <Self as System>::SystemData) {
        let cache_opt = self.caches.get(&client_id);
        if cache_opt.is_none() {
            warn!("Trying to process Msg for client {} which is not in input cache, \
                   probably disconnected, skipping.", client_id);
            return;
        }

        let cache = cache_opt.expect(
            &format!("Failed to get input cache for client {}", client_id));

        match msg {
            Msg::Walk(direction) => {
                if let Some(controlled) = cache.entity_control {
                    self.action_s.send(Action::Walk(
                        WalkAction { entity: controlled, direction: direction }))
                        .expect("Failed to send WalkAction.");
                }
            },
            _ => warn!("Throwing away message {:?}", msg),
        }
    }
}

impl<'a> System<'a> for NetInputSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Client>,
        WriteStorage<'a, ClientControl>,
        WriteStorage<'a, ClientView>,
        ReadStorage<'a, EntityType>,
    );

    fn run(&mut self, mut data: Self::SystemData) {
        self.register_and_unregister_clients(&mut data);
        self.update_input_cache(&mut data);
        self.process_msgs(&mut data);
    }
}


struct ClientInputCache {
    entity_control: Option<Entity>,
}

impl ClientInputCache {
    pub fn new() -> Self {
        ClientInputCache {
            entity_control: None,
        }
    }
}
