use crossbeam_channel::Sender;
use ws;
use std::thread::{self, JoinHandle};

use common::types::ClientId;
use common::protocol::Msg;
use super::socket::{ClientReceiverRegistration, ClientSenderRegistration};

pub struct Client {
    id: ClientId,
    state: ClientState,
    to_server: Sender<Msg>,
    from_server_thread: Option<JoinHandle<()>>,
    client_receiver_registrar: Sender<ClientReceiverRegistration>,
    client_sender_registrar: Sender<ClientSenderRegistration>,
}

impl Client {
    pub fn new(id: ClientId, ws_sender: ws::Sender,
               client_receiver_registrar: Sender<ClientReceiverRegistration>,
               client_sender_registrar: Sender<ClientSenderRegistration>) -> Self {
        // create channels both ways and register on the ECS side
        let (to_server, from_client) = crossbeam_channel::unbounded::<Msg>();
        let (to_client, from_server) = crossbeam_channel::unbounded::<Msg>();
        client_receiver_registrar.send(ClientReceiverRegistration::Register(id, from_client))
            .expect("Failed to register connected client (receiver)");
        client_sender_registrar.send(ClientSenderRegistration::Register(id, to_client))
            .expect("Failed to register connected client (sender)");

        // thread to encode and push data from ECS to WebSocket
        let from_server_thread = thread::spawn(move || {
            loop {
                if let Ok(msg) = from_server.recv() {
                    trace!("Sending message {:?} to client {}", msg, id);
                    let msg_bin = msg.to_bincode()
                        .expect("Failed to convert outgoing Msg to bincode");
                    ws_sender.send(ws::Message::Binary(msg_bin))
                        .expect("Failed to send WebSocket message");
                } else {
                    break;
                }
            }
        });

        // return handler to be used for receiving from WebSocket to ECS
        Client {
            id: id,
            state: ClientState::New,
            to_server: to_server,
            from_server_thread: Some(from_server_thread),
            client_receiver_registrar: client_receiver_registrar,
            client_sender_registrar: client_sender_registrar,
        }
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        info!("Tearing down WebSocket client {}", self.id);
        self.client_receiver_registrar.send(ClientReceiverRegistration::Unregister(self.id))
            .expect("Failed to unregister client receiver from ECS");
        self.client_sender_registrar.send(ClientSenderRegistration::Unregister(self.id))
            .expect("Failed to unregister client sender from ECS");
        if let Some(join_handle) = self.from_server_thread.take() {
            join_handle.join().expect("Failed to join WebSocket sender thread");
        }
        info!("Reaping WebSocket client {}", self.id);
    }
}

impl ws::Handler for Client {
    fn on_open(&mut self, _: ws::Handshake) -> ws::Result<()> {
        Ok(())
    }

    fn on_message(&mut self, msg: ws::Message) -> ws::Result<()> {
        match msg {
            ws::Message::Binary(data) => {
                match Msg::from_bincode(&data) {
                    Ok(msg) => self.to_server.send(msg)
                        .expect("Failed to transfer message from websocket to game engine"),
                    Err(e) => warn!("Invalid message from client {} received: {}", self.id, e),
                }
            },
            _ => {},
        }
        Ok(())
    }

    fn on_close(&mut self, code: ws::CloseCode, reason: &str) {
        match code {
            ws::CloseCode::Normal | ws::CloseCode::Away => {
                info!("Closed client connection {}", self.id);
            }
            ws::CloseCode::Abnormal => warn!(
                "Closing handshake failed! Unable to obtain closing status from client."),
            _ => warn!("The client {} encountered an error: {}", self.id, reason),
        }
    }

    fn on_error(&mut self, err: ws::Error) {
        error!("The websocket server encountered an error: {:?}", err);
    }
}

pub enum ClientState {
    New,
    EntityControl,
}
