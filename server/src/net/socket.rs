use crossbeam_channel::{Sender, Receiver};
use ws;
use std::thread;

use super::client::Client;
use common::protocol::Msg;
use common::types::ClientId;

const WS_LISTEN_ADDR_PORT: &'static str = "127.0.0.1:3777";

pub fn spawn_ws_server(client_receiver_registrar: Sender<ClientReceiverRegistration>,
                       client_sender_registrar: Sender<ClientSenderRegistration>) {
    thread::spawn(|| {
        ws_server(client_receiver_registrar, client_sender_registrar);
    });
}

fn ws_server(client_receiver_registrar: Sender<ClientReceiverRegistration>,
             client_sender_registrar: Sender<ClientSenderRegistration>) {
    let mut client_id_counter: ClientId = 0;
    let spawn_client = |ws_sender: ws::Sender| {
        client_id_counter += 1;
        let id = client_id_counter;
        Client::new(id, ws_sender,
                    client_receiver_registrar.clone(), client_sender_registrar.clone())
    };

    ws::listen(WS_LISTEN_ADDR_PORT, spawn_client).expect("Error when running websocket server");
}

pub enum ClientReceiverRegistration {
    Register(ClientId, Receiver<Msg>),
    Unregister(ClientId),
}

pub enum ClientSenderRegistration {
    Register(ClientId, Sender<Msg>),
    Unregister(ClientId),
}
