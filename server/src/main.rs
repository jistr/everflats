#[macro_use]
extern crate log;

extern crate crossbeam_channel;
extern crate env_logger;
extern crate everflats_common as common;
extern crate specs;
extern crate shred;
extern crate ws;

mod action;
mod entity;
mod component;
mod net;
mod state;
mod system;

use common::types::*;
use crate::state::EfWorld;
use crate::net::socket::{self, ClientReceiverRegistration, ClientSenderRegistration};

fn main() -> Result<()> {
    env_logger::Builder::from_default_env()
        .write_style(env_logger::WriteStyle::Never)
        .init();

    // FIXME: replace all unbounded channels with bounded ones
    let (client_receiver_s, client_receiver_r) =
        crossbeam_channel::unbounded::<ClientReceiverRegistration>();
    let (client_sender_s, client_sender_r) =
        crossbeam_channel::unbounded::<ClientSenderRegistration>();

    let mut world = EfWorld::new(client_receiver_r, client_sender_r)?;
    socket::spawn_ws_server(client_receiver_s, client_sender_s);

    world.run();
    Ok(())
}
