use std::collections::hash_map::DefaultHasher;
use std::hash::Hasher;

use common::consts::entity_t;
use entity::{terrain, client};
use super::*;

pub struct InitState {}

impl EfState for InitState {
    fn setup(&mut self, _: &mut Dispatcher, world: &mut World) {
        info!("Initializing.");

        self.init_terrain(world);
    }

    fn update(&mut self, _dispatcher: &mut Dispatcher, _world: &mut World)
              -> Option<Box<dyn EfState>> {
        Some(Box::new(RunningState {}))
    }
}

impl InitState {
    fn init_terrain(&mut self, world: &mut World) {
        // FIXME: just hardwiring some terrain before proper loading
        // or generation is implemented

        let terrain_type = |x, y| {
            let mut hasher = DefaultHasher::new();
            hasher.write_i32(x);
            hasher.write_i32(y);
            match hasher.finish() % 7 {
                0 => entity_t::T_STONE,
                _ => entity_t::T_DIRT,
            }
        };

        for x in -100..100 {
            for y in -100..100 {
                terrain::build_terrain(world.create_entity(), x, y, terrain_type(x, y));
            }
        }
        client::build_admin_view(world.create_entity());
    }
}
