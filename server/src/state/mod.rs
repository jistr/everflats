use crate::action::Action;
use crate::component;
use crate::net::socket::{ClientReceiverRegistration, ClientSenderRegistration};
use crate::system::*;
use common::types::*;
use crossbeam_channel::{self, Receiver};
use specs::{World, Dispatcher, DispatcherBuilder};

// FIXME reintroduce states
pub mod init;
pub mod running;

pub use self::init::InitState;
pub use self::running::RunningState;

pub struct EfWorld<'a, 'b> {
    pub dispatcher: Dispatcher<'a, 'b>,
    pub world: World,
    state: Box<dyn EfState>,
}

impl<'a, 'b> EfWorld<'_, '_> {
    pub fn new(client_receiver_r: Receiver<ClientReceiverRegistration>,
               client_sender_r: Receiver<ClientSenderRegistration>) -> Result<Self> {
        let (action_s, action_r) = crossbeam_channel::unbounded::<Action>();

        let net_input_sys = NetInputSystem::new(action_s, client_receiver_r);
        let action_sys = ActionSystem::new(action_r);
        let net_output_sys = NetOutputSystem::new(client_sender_r);

        let mut world = World::new();
        world.register::<component::Client>();
        world.register::<component::ClientControl>();
        world.register::<component::ClientView>();
        world.register::<component::EntityType>();
        world.register::<component::Location>();

        let mut dispatcher = DispatcherBuilder::new()
            .with(net_input_sys, "net input", &[])
            .with(action_sys, "action", &["net input"])
            .with(net_output_sys, "net output", &["action"])
            .build();

        let mut init_state = InitState {};
        init_state.setup(&mut dispatcher, &mut world);
        Ok(EfWorld {
            dispatcher: dispatcher,
            world: world,
            state: Box::new(init_state),
        })
    }

    pub fn run(&mut self) {
        loop {
            let new_state_opt = self.state.update(&mut self.dispatcher, &mut self.world);
            if let Some(mut new_state) = new_state_opt {
                self.state.teardown(&mut self.dispatcher, &mut self.world);
                new_state.setup(&mut self.dispatcher, &mut self.world);
                self.state = new_state;
            }
        }
    }
}

pub trait EfState {
    fn setup(&mut self, _dispatcher: &mut Dispatcher, _world: &mut World) {}
    fn teardown(&mut self, _dispatcher: &mut Dispatcher, _world: &mut World) {}
    fn update(&mut self, _dispatcher: &mut Dispatcher, _world: &mut World)
              -> Option<Box<dyn EfState>>;
}
