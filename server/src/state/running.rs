use std::cmp::max;
use std::thread::sleep;
use std::time::{Duration, Instant};

use super::*;

const TICK_DURATION_MS: u32 = 200;

pub struct RunningState {}

impl EfState for RunningState {
    fn setup(&mut self, _: &mut Dispatcher, world: &mut World) {
        info!("Running.");
        world.add_resource(Tick::start_now())
    }

    fn update(&mut self, dispatcher: &mut Dispatcher, world: &mut World)
              -> Option<Box<dyn EfState>> {
        let previous_tick = world.read_resource::<Tick>().clone();
        previous_tick.sleep_till_end();

        trace!("Tick.");
        let tick = Tick::start_now();
        world.add_resource(tick.clone());
        dispatcher.dispatch(&mut world.res);
        world.maintain();
        if tick.is_overtime() {
            warn!("World tick took too much time: wanted {} ms but it took {} ms.",
                  TICK_DURATION_MS, tick.millis_since_start());
        }
        None
    }
}

#[derive(Clone)]
pub struct Tick {
    pub tick_start: Instant,
}

impl Tick {
    pub fn start_now() -> Self {
        Tick {
            tick_start: Instant::now(),
        }
    }

    pub fn is_overtime(&self) -> bool {
        let since_start = Instant::now().duration_since(self.tick_start);
        since_start.as_secs() >= (TICK_DURATION_MS as u64 / 1000) &&
            since_start.subsec_millis() > (TICK_DURATION_MS % 1000)
    }

    pub fn millis_since_start(&self) -> u64 {
        let since_start = Instant::now().duration_since(self.tick_start);
        since_start.as_secs() * 1000 + since_start.subsec_millis() as u64
    }

    pub fn millis_to_end_trimmed(&self) -> u64 {
        let since_start = self.millis_since_start();
        max(0, TICK_DURATION_MS as i64 - since_start as i64) as u64
    }

    pub fn sleep_till_end(&self) {
        // NOTE: this may result in sleeping slightly longer and thus
        // ticks not being always exactly TICK_DURATION_MS long, but
        // in Everflats that shouldn't matter much.
        sleep(Duration::from_millis(self.millis_to_end_trimmed()));
    }
}
