Known Issues
============

* All entities get sent to clients, even those which should be
  invisible, e.g. `S_ADMIN_VIEW`.
