Milestones
==========

Plan
----

### 4

* Optimize location-based queries somehow, probably via some
  cache. The game will use this a lot so it needs to be
  efficient. E.g.:

  * What's in my line of sight (net_output system already queries
    this, currently very inefficiently).

  * What's in that direction from this entity. (Can i move that way?)

  * What's on my tile, e.g. what kind of surface am i standing on, am
    i swimming, etc.

  * Various area-effect events and world-shaping cellular automata
    need to query neighbors and areas too.

### 5

* Optimize client-server protocol to send less data. Compare between
  two world clock ticks and send some form of diff.

  * Rectangle diff: consider what area the client saw in previous
    tick, what area client should see now, send all entities in the
    newly exposed part.

  * Active entities diff - in addition to rectangle diff, send
    entities for which the following is true:

    * AND:

      * The entity has changed (e.g. moved) this tick.

      * OR:

        * The entity is in current line of sight in this tick. In this
          case resend the entity data. (Eventually we may work out a
          diff-based method on entity level too, but not in this
          milestone.)

        * The entity was in previous line of sight in previous tick,
          but is no longer in current line of sight this tick. In this
          case send a message to forget the entity. (This is important
          to correctly handle entities leaving clients LoS or
          teleporting or getting killed or processed in some action,
          e.g. crafitng etc.)

### 6

* Basic algorithm for generating the world. Grass and water.

### 7

* Drive generation of world chunks by presence of the admin entity.


Important TODOs
---------------

* WUI - reap data for entities which are beyond vision range

* Add credits to be able to credit e.g. game-icons.net


Idea Heap
---------

* Persistent saving of the world state.


Past Milestones
---------------

### 1

* Small hardcoded world comprising of dirt and stone.

* Basic render of the world in WUI.

* Minimal communication between server and WUI to achieve the world
  rendering.

### 2

* Create an intangible entity that WUI will attach the rendering to.

* Allow moving the entity around via keys, redraw WUI accordingly.

### 3

* Slim down the server. We don't need the whole amethyst framework,
  perhaps just `amethyst_core` would be enough or even just `specs`,
  the ECS framework.
