Everflats - design doc
======================

Executive summary
-----------------

Everflats is a 2D top-down tile-based sandbox game. When compared to
other similar games, Everflats graphics and physics are reduced to a
bare minimum, while complex system [^cs] emergent gameplay and
enablement of sophisticated AI actors is accented.

[^cs] https://en.wikipedia.org/wiki/Complex_system

Key concepts
------------

* The world is fully shapeable by actors and by the world itself.

* The world is auto-generated.

* No actors get "special treatment" in the world engine. AI actors use
  the same inputs/outputs to act on the world as the players do.

* Actor gets a set of capabilities. This is what will differentiate
  creatures (actor types) from each other and hopefully allow for
  creating an interesting/diverse world.

  * However, one more emphasis on the "no special AI treatment in game
    engine" rule is in order here. The ultimate test is that each AI
    creature should in theory be controllable by a human-UI client and
    still have the same capabilities (and vice versa re humans being
    replaced by AI this way).

* Actor gets a set of ambitions and is continuously scored on
  those.

  * This is especially important for AI, as that's what can drive
    reinforcement learning algorithms to form behaviors of
    creatures.

  * It is likely that there will be creatures with predefined
    combination of capabilities + ambitions + pre-trained "brain".

* Various ambition sets could be created to result in vastly different
  game experiences, but the initial focus will go towards some
  domination or king-of-the-hill type of ambitions.

Ideas for later stages
----------------------

* Initially the game will probably be very simple, featuring some kind
  of contention over resources and rudimentary combat. Subsequently
  more possibilities of shaping the world and various gameplay styles
  should be added.

* As no actors are special from the point of view of the world engine,
  players could control ("connect as") a wide range of creatures.

* Some rules could be made dynamic/adaptable to make sure the world is
  truly self-balancing. However this kinda goes against someone being
  able to "win" in the game. Assuming the game goal is some form of
  domination/KotH, winning means throwing the game off equilibrium.

* It is not yet clear whether the AI brains should be fully
  pre-trained or also adapting as game progresses. Adapting would
  probably be considerably more work and it might spiral out into
  nonsense, so i'm leaning rather against it.

Technical details
-----------------

* Client-server based architecture.

* Both the players and AI are clients, the only difference is that
  player client has some form of UI and is controlled by a human,
  while the AI client is non-human and can be "headless".

* Given that UI creation is pain (for me), and the only thing that
  makes it remotely bearable (for me) is HTML, the player client will
  be web-based.

  * This should be fine performance-wise as the graphics requirements
    are next to none.

  * A bit unfortunately, this probably locks the client-server
    protocol into using websockets (TCP). This is what i'm slightly
    more worried about, but AFAIU not much can be done about it.
