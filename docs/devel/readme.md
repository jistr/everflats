Development workflow
--------------------

Build a toolbox container image:

    ./toolbox/build

Start the toolbox container in the background:

    ./toolbox/start

Install web ui prerequisites:

    ./toolbox/exec make prereq-wui

Start development loop scripts (in parallel) to launch the game
server, and a web server serving the Web UI, and tests:

    ./toolbox/exec ./tools/devel-loop-server.sh

    ./toolbox/exec ./tools/devel-loop-wui.sh

Then point your browser to `http://localhost:8080` and the Web UI
should load and connect you to the local server.

To run the unit tests in a loop, you can use:

    ./toolbox/exec ./tools/devel-loop-test.sh
