#![recursion_limit = "512"]

extern crate everflats_common as common;

mod game;
mod diagnostic;
mod input;
mod net;
mod render;
mod ui;
mod util;
mod var;

pub use ui::app::App;
pub use game::GameWorker;

#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
