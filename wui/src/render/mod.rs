use crate::game::RenderData;
use common::consts::*;
use common::types::*;
use web_sys::{self, Document, Element};

pub fn prepare_world(world: &Element) -> Result<()> {
    let doc = world.owner_document().expect("Cannot get owner doc for world element");
    let grid_size = VISION_RANGE * 2 + 1;
    for _y in 0..(grid_size as u32) {
        let tr = doc.create_element("tr")?;
        for _x in 0..(grid_size as u32) {
            let td = doc.create_element("td")?;
            tr.append_child(&td)?;
        }
        world.append_child(&tr)?;
    }
    Ok(())
}

pub fn render_world(rd: &RenderData, world: &Element) -> Result<()> {
    let doc = world.owner_document().expect("Cannot get owner doc for world element");
    let world_rows = world.children();

    let grid_size = VISION_RANGE * 2 + 1;
    for y in 0..(grid_size as u32) {
        let y_offset = -(y as i32) + VISION_RANGE;

        let row = world_rows.get_with_index(y).ok_or_else(
            || Error::render("Failed to get world row"))?;

        render_world_row(rd, y_offset, &row, &doc)?;
    }

    Ok(())
}

fn render_world_row(rd: &RenderData, y_offset: Coord, row: &Element, doc: &Document)
                    -> Result<()> {
    let grid_size = VISION_RANGE * 2 + 1;
    let row_cells = row.children();
    for x in 0..(grid_size as u32) {
        let x_offset = x as i32 - VISION_RANGE;

        let cell = row_cells.get_with_index(x).ok_or_else(
            || Error::render("Failed to get world cell"))?;
        render_world_cell(rd, x_offset, y_offset, &cell, &doc)?;
    }
    Ok(())
}

fn render_world_cell(rd: &RenderData, x_offset: Coord, y_offset: Coord, cell: &Element,
                     doc: &Document) -> Result<()> {
    while let Some(child_node) = cell.first_child() {
        cell.remove_child(&child_node)?;
    }
    let x = rd.vision_center.x + x_offset;
    let y = rd.vision_center.y + y_offset;
    if let Some(bucket) = rd.location_entities_cache.get(&Location {x: x, y: y}) {
        for entity in bucket.iter() {
            let rendered = render_entity(&entity, doc)?;
            cell.append_child(&rendered)?;
        }
    }
    Ok(())
}

fn render_entity(entity: &Entity, doc: &Document) -> Result<Element> {
    let rendered = doc.create_element("span")?;
    rendered.set_attribute("class", &format!("ent_{}", entity_t::name(entity.entity_type)))?;
    Ok(rendered)
}
