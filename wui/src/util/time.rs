use instant::Instant;
use std::time::Duration;
use yew::callback::Callback;
use yew::services::Task;
use yew::services::timeout::TimeoutService;

pub fn new_fps_timer(callback: Callback<()>, last_update: Instant, max_fps: u64)
                     -> Box<dyn Task> {
    let since_last_update = Instant::now().duration_since(last_update);
    let ideal_period = Duration::from_millis(1000 / max_fps);
    let remaining_period = match ideal_period.checked_sub(since_last_update) {
        Some(period) => period,
        None => Duration::from_millis(0),
    };
    Box::new(TimeoutService::spawn(remaining_period, callback))
}
