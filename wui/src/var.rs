use std::cell::RefCell;
use std::collections::HashMap;
use wasm_bindgen::prelude::JsValue;

thread_local!(static VARS: RefCell<Option<HashMap<String, JsValue>>> = RefCell::new(None));

pub fn reset_all() {
    VARS.with(|vars_rc| {
        let mut vars_opt = vars_rc.borrow_mut();
        vars_opt.take();

        vars_opt.get_or_insert(HashMap::new());
    });

    set("debug", false.into());
}

pub fn try_get(key: &str) -> Option<JsValue> {
    VARS.with(|vars_rc| {
        let mut vars_opt = vars_rc.borrow_mut();
        let vars = vars_opt.as_mut().unwrap();
        vars.get(key).cloned()
    })
}

pub fn get(key: &str) -> JsValue {
    try_get(key).expect(&format!("VARS undefined key '{}'", key))
}

#[allow(dead_code)]
pub fn get_bool(key: &str) -> bool {
    get(key).as_bool().expect(&format!("VARS key '{}' is not bool", key))
}

pub fn get_f64(key: &str) -> f64 {
    get(key).as_f64().expect(&format!("VARS key '{}' is not a number", key))
}

pub fn set(key: &str, val: JsValue) {
    VARS.with(|vars_rc| {
        let mut vars_opt = vars_rc.borrow_mut();
        let vars = vars_opt.as_mut().unwrap();
        vars.insert(key.into(), val);
    });
}
