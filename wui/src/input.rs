use common::protocol::Msg;
use common::types::Direction;
use log::*;

pub type Keycode = u32;

pub const KEYCODE_UP: Keycode = 87;
pub const KEYCODE_DOWN: Keycode = 83;
pub const KEYCODE_LEFT: Keycode = 65;
pub const KEYCODE_RIGHT: Keycode = 68;

pub struct InputState {
    pub direction_down: bool,
    pub direction_left: bool,
    pub direction_right: bool,
    pub direction_up: bool,
}

impl InputState {
    pub fn new() -> Self {
        InputState {
            direction_down: false,
            direction_left: false,
            direction_right: false,
            direction_up: false,
        }
    }

    pub fn set_key_down(&mut self, keycode: Keycode) {
        match keycode {
            KEYCODE_DOWN => self.direction_down = true,
            KEYCODE_LEFT => self.direction_left = true,
            KEYCODE_RIGHT => self.direction_right = true,
            KEYCODE_UP => self.direction_up = true,
            _ => (),
        }
    }

    pub fn set_key_up(&mut self, keycode: Keycode) {
        match keycode {
            KEYCODE_DOWN => self.direction_down = false,
            KEYCODE_LEFT => self.direction_left = false,
            KEYCODE_RIGHT => self.direction_right = false,
            KEYCODE_UP => self.direction_up = false,
            _ => (),
        }
    }

    pub fn produce_input_msgs(&self) -> Vec<Msg> {
        let mut msgs = vec![];
        // Deliberately not handling multiple conflicting keys pressed
        // for now, the server must handle this anyway to prevent hacks.
        if self.direction_up {
            msgs.push(Msg::Walk(Direction::N));
        }
        if self.direction_left {
            msgs.push(Msg::Walk(Direction::W));
        }
        if self.direction_right {
            msgs.push(Msg::Walk(Direction::E));
        }
        if self.direction_down {
            msgs.push(Msg::Walk(Direction::S));
        }
        trace!("Input messages: {:?}", msgs);
        msgs
    }
}
