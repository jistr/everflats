use js_sys::Date;
use crate::var;

const BPS_REFRESH_SECONDS: f64 = 2_f64;

const BPS: &'static str = "diag_bps";
const BPS_COUNTER: &'static str = "diag_bps_counter";
const BPS_LAST_SNAPSHOT: &'static str = "diag_bps_last_snapshot";

pub fn reset_all() {
    var::set(BPS, 0.into());
    var::set(BPS_COUNTER, 0.into());
    var::set(BPS_LAST_SNAPSHOT, timestamp().into());
}

pub fn received_bytes(bytes_received: usize) {
    let now = timestamp();
    let last_snapshot = var::get_f64(BPS_LAST_SNAPSHOT);
    let mut current_bytes = var::get_f64(BPS_COUNTER);
    if now - last_snapshot > BPS_REFRESH_SECONDS {
        var::set(BPS, (current_bytes / (now - last_snapshot)).into());
        current_bytes = 0_f64;
        var::set(BPS_LAST_SNAPSHOT, now.into());
    }

    current_bytes += bytes_received as f64;
    var::set(BPS_COUNTER, current_bytes.into());
}

fn timestamp() -> f64 {
    // UNIX timestamp seconds as f64
    Date::now() / 1000_f64
}
