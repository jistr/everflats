use crate::game::RenderData;
use crate::render;
use log::*;
use std::rc::Rc;
use web_sys::Element;
use yew::prelude::*;

#[derive(Clone, Properties)]
pub struct Props {
    pub render_data: Rc<RenderData>,
}

impl Default for Props {
    fn default() -> Self {
        Self {
            render_data: Rc::new(RenderData::default()),
        }
    }
}

pub struct GameScreen {
    #[allow(dead_code)]
    link: ComponentLink<Self>,
    props: Props,
    world_ref: NodeRef,
}

pub enum Msg {
}

impl Component for GameScreen {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link: link,
            props: props,
            world_ref: NodeRef::default(),
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if Rc::ptr_eq(&props.render_data, &self.props.render_data) {
            return false;
        }

        self.props.render_data = props.render_data;
        true
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        trace!("view");
        html! {
          <main class="h-100 container-fluid flex-fill">
            <div class="row h-100">
              <div class="d-flex col-9 bg-white p-0">
                <table ref=self.world_ref.clone() id="world" class="world w-100">
                </table>
              </div>
              <div class="col-3 bg-light"></div>
            </div>
          </main>
        }
    }

    fn rendered(&mut self, first_render: bool) {
        let world = self.world_ref.cast::<Element>().expect("Cannot get world element");
        if first_render {
            if let Err(e) = render::prepare_world(&world) {
                error!("Error preparing world rendering: {}", e);
            }
        }
        if let Err(e) = render::render_world(&self.props.render_data, &world) {
            error!("Error rendering world: {}", e);
        }
    }
}
