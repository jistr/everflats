pub(super) mod app;
mod game_screen;
mod main_footer;
mod main_header;

use game_screen::GameScreen;
use main_footer::MainFooter;
use main_header::MainHeader;
