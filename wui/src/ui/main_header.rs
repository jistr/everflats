use ::log::*;
use ::yew::prelude::*;

pub struct MainHeader {
    #[allow(dead_code)]
    link: ComponentLink<Self>,
}

pub enum Msg {
}

impl Component for MainHeader {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link: link,
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        trace!("MainHeader render");
        html! {
          <nav class="navbar navbar-dark bg-dark">
            <span class="navbar-brand">{ "Everflats" }</span>
            <span class="status_class">{ "status_text" }</span>
          </nav>
        }
    }
}
