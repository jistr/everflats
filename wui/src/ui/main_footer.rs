use ::log::*;
use ::yew::prelude::*;

pub struct MainFooter {
    #[allow(dead_code)]
    link: ComponentLink<Self>,
}

pub enum Msg {
}

impl Component for MainFooter {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link: link,
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        trace!("MainFooter render");
        html! {
          <nav class="navbar navbar-dark bg-dark p-1">
            <span class="text-light">{ "\u{00a0}" }</span>
          </nav>
        }
    }
}
