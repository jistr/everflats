use crate::game::{RenderData, GameWorker, GameWorkerRequest, GameWorkerResponse};
use crate::ui::{GameScreen, MainFooter, MainHeader};
use crate::util::time::new_fps_timer;
use instant::Instant;
use log::*;
use std::rc::Rc;
use yew::prelude::*;
use yew::services::Task;
use yew::services::keyboard::{KeyboardService, KeyListenerHandle};

const MAX_FPS: u64 = 30;

pub struct App {
    link: ComponentLink<Self>,
    game: Box<dyn Bridge<GameWorker>>,
    render_data: Rc<RenderData>,
    world_render_task: Box<dyn Task>,
    last_world_render: Instant,
    _keydown_handle: KeyListenerHandle,
    _keyup_handle: KeyListenerHandle,
}

pub enum Msg {
    GameWorker(GameWorkerResponse),
    KeyDown(KeyboardEvent),
    KeyUp(KeyboardEvent),
    RequestRenderData,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let game_worker_callback = link.callback(Msg::GameWorker);
        let last_world_render = Instant::now();
        let world_render_task = new_fps_timer(
            link.callback(|_| Msg::RequestRenderData), last_world_render, MAX_FPS);
        let document = yew::utils::document();
        let keydown_handle = KeyboardService::register_key_down(
                &document, link.callback(|ev| Msg::KeyDown(ev)));
        let keyup_handle = KeyboardService::register_key_up(
                &document, link.callback(|ev| Msg::KeyUp(ev)));

        Self {
            link: link,
            game: GameWorker::bridge(game_worker_callback),
            render_data: Rc::new(RenderData::default()),
            world_render_task: world_render_task,
            last_world_render: last_world_render,
            _keydown_handle: keydown_handle,
            _keyup_handle: keyup_handle,
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::GameWorker(response) => self.game_worker_msg(response),
            Msg::KeyDown(ev) => self.on_key_down(ev),
            Msg::KeyUp(ev) => self.on_key_up(ev),
            Msg::RequestRenderData => self.request_render_data(),
        }
    }

    fn view(&self) -> Html {
        trace!("view");
        let mut game_screen_props = <GameScreen as Component>::Properties::default();
        game_screen_props.render_data = self.render_data.clone();
        html! {
          <div class="app container d-flex flex-column p-0">
            <MainHeader/>
            <GameScreen with game_screen_props/>
            <MainFooter/>
          </div>
        }
    }

    fn rendered(&mut self, first_render: bool) {
        if first_render {
        }
    }
}

impl App {
    fn game_worker_msg(&mut self, response: GameWorkerResponse) -> ShouldRender {
        trace!("game_worker_msg");
        match response {
            GameWorkerResponse::RenderData(data) => self.render_game(data),
        }
    }

    fn request_render_data(&mut self) -> ShouldRender {
        trace!("request_render_data");
        self.last_world_render = Instant::now();
        self.world_render_task = new_fps_timer(
            self.link.callback(|_| Msg::RequestRenderData), self.last_world_render, MAX_FPS);
        self.game.send(GameWorkerRequest::GetRenderData);
        false  // here we just request RenderData, we don't have it yet
    }

    fn render_game(&mut self, render_data: RenderData) -> ShouldRender {
        trace!("render_game");
        self.render_data = Rc::new(render_data);
        true
    }

    fn on_key_down(&mut self, event: KeyboardEvent) -> ShouldRender {
        self.game.send(GameWorkerRequest::KeyDown(event.key_code()));
        false
    }

    fn on_key_up(&mut self, event: KeyboardEvent) -> ShouldRender {
        self.game.send(GameWorkerRequest::KeyUp(event.key_code()));
        false
    }
}
