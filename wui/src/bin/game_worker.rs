use log::*;
use yew::agent::Threaded;

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    info!("everflats_wasm::GameWorker starting");
    everflats_wasm::GameWorker::register();
}
