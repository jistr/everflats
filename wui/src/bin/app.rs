use log::*;

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    info!("everflats_wasm::App starting");
    yew::start_app::<everflats_wasm::App>();
}
