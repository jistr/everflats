use common::types::*;
use serde_derive::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct RenderData {
    pub entities: EntitiesMap,
    pub vision_center: Location,
    pub location_entities_cache: LocationEntitiesMap,
}

impl Default for RenderData {
    fn default() -> Self {
        Self {
            entities: EntitiesMap::new(),
            vision_center: Location {x: 0, y: 0},
            location_entities_cache: LocationEntitiesMap::new(),
        }
    }
}
