use crate::var;
use crate::diagnostic;
use crate::net::Connection;
use crate::game::{GameState, RenderData};
use crate::input::{Keycode, InputState};
use crate::util::time::new_fps_timer;
use common::types::*;
use instant::Instant;
use log::*;
use serde_derive::{Deserialize, Serialize};
use yew::services::Task;
use yew::worker::*;

const MAX_FPS: u64 = 30;

#[derive(Serialize, Deserialize, Debug)]
pub enum Request {
    GetRenderData,
    KeyDown(Keycode),
    KeyUp(Keycode),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Response {
    RenderData(RenderData),
}

pub enum Msg {
    UpdateWorld,
}

pub struct GameWorker {
    link: AgentLink<Self>,

    conn: Connection,
    game: GameState,
    input: InputState,
    last_world_update: Instant,
    world_update_task: Box<dyn Task>,
}

impl Agent for GameWorker {
    type Reach = Public<Self>;
    type Message = Msg;
    type Input = Request;
    type Output = Response;

    fn create(link: AgentLink<Self>) -> Self {
        var::reset_all();
        diagnostic::reset_all();

        let last_world_update = Instant::now();
        let world_update_task = new_fps_timer(
            link.callback(|_| Msg::UpdateWorld), last_world_update, MAX_FPS);
        let mut worker = Self {
            link,

            conn: Connection::new(),
            game: GameState::new(),
            input: InputState::new(),
            last_world_update: last_world_update,
            world_update_task: world_update_task,
        };

        match worker.conn.connect("ws://localhost:3777") {
            Ok(_) => {},
            Err(e) => error!("Error when connecting to server: {:?}", e),
        }

        worker
    }

    fn update(&mut self, msg: Self::Message) {
        match msg {
            Msg::UpdateWorld => {
                self.last_world_update = Instant::now();
                if let Err(e) = self.update_world() {
                    error!("Error updating world state: {}", e);
                }
                self.world_update_task = new_fps_timer(
                    self.link.callback(|_| Msg::UpdateWorld), self.last_world_update, MAX_FPS);
            }
        }
    }

    fn handle_input(&mut self, msg: Self::Input, who: HandlerId) {
        trace!("handle_input: {:?}", msg);
        match msg {
            Self::Input::GetRenderData => {
                if self.game.should_render() {
                    let rd = self.game.create_render_data();
                    self.link.respond(who, Self::Output::RenderData(rd));
                }
            },
            Self::Input::KeyDown(keycode) => self.input.set_key_down(keycode),
            Self::Input::KeyUp(keycode) => self.input.set_key_up(keycode),
        }
    }

    fn name_of_resource() -> &'static str {
        "game_worker.js"
    }
}

impl GameWorker {
    fn update_world(&mut self) -> Result<()> {
        trace!("update_world");
        for msg in self.input.produce_input_msgs().into_iter() {
            self.conn.send(msg)?;
        }
        while let Some(msg) = self.conn.receive()? {
            self.game.process_incoming_msg(msg)?;
        }
        Ok(())
    }
}
