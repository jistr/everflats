use crate::game::RenderData;
use common::protocol::Msg;
use common::types::*;
use log::*;
use std::collections::{HashMap, VecDeque};

pub struct GameState {
    // All entities we are keeping track of.
    entities: EntitiesMap,
    vision_center: Location,
    // whether there was a new batch processed, yet to be rendered in UI
    should_render: bool,

    // Messages get buffered here until we receive a BatchEnd. The
    // idea is that we always want to process full batches before we
    // update the UI.
    batch_msg_buffer: VecDeque<Msg>,
    // Bucket of entities for each location where at least one exists.
    location_entities_cache: LocationEntitiesMap,
}

impl GameState {
    pub fn new() -> Self {
        GameState {
            entities: HashMap::new(),
            vision_center: Location { x: 0, y: 0},
            should_render: false,

            batch_msg_buffer: VecDeque::new(),
            location_entities_cache: HashMap::new(),
        }
    }

    pub fn process_incoming_msg(&mut self, msg: Msg) -> Result<()> {
        if let Msg::BatchEnd = msg {
            while let Some(msg) = self.batch_msg_buffer.pop_front() {
                match msg {
                    Msg::Entities(entities) => self.process_msg_entities(entities)?,
                    Msg::VisionCenter(vc) => self.process_msg_vision_center(vc)?,
                    _ => { warn!("Throwing away unrecognized message {:?}", msg) },
                }
            }
            self.process_msg_batch_end()?;
        } else {
            self.batch_msg_buffer.push_back(msg);
        }
        Ok(())
    }

    pub fn should_render(&self) -> bool {
        self.should_render
    }

    pub fn create_render_data(&mut self) -> RenderData {
        self.should_render = false;
        RenderData {
            entities: self.entities.clone(),
            vision_center: self.vision_center,
            location_entities_cache: self.location_entities_cache.clone(),
        }
    }

    fn process_msg_entities(&mut self, entities: Vec<Entity>) -> Result<()> {
        trace!("Received {} entities", entities.len());
        for entity in entities.into_iter() {
            self.entities.insert(entity.id, entity);
        }
        Ok(())
    }

    fn process_msg_vision_center(&mut self, vision_center: Location) -> Result<()> {
        trace!("Received vision center {:?}", vision_center);
        self.vision_center = vision_center;
        Ok(())
    }

    fn process_msg_batch_end(&mut self) -> Result<()> {
        trace!("Received batch end");
        self.update_location_entities_cache();
        self.should_render = true;
        Ok(())
    }

    fn update_location_entities_cache(&mut self) {
        let mut le: LocationEntitiesMap = HashMap::new();
        for entity in self.entities.values() {
            if le.get(&entity.location).is_none() {
                le.insert(entity.location.clone(), Vec::new());
            }
            let bucket = le.get_mut(&entity.location)
                .expect("Failed to get entity location bucket");
            bucket.push(entity.clone());
        }
        // Sort entities in each bucket by entity_type, which
        // corresponds with rendering order from lowest to hightest
        // priority.
        for bucket in le.values_mut() {
            bucket.sort_unstable_by_key(|entity| entity.entity_type);
        }
        self.location_entities_cache = le;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use common::consts::entity_t;

    fn located_entity(id: EntityId, et: EntityT, x: Coord, y: Coord) -> Entity {
        Entity {
            id: id,
            entity_type: et,
            location: Location { x: x, y: y},
        }
    }

    #[test]
    fn test_update_location_entities_cache() {
        let mut gd = GameState::new();
        let ent1 = located_entity(1, entity_t::P_GRASS_SHORT, 4, 5);
        let ent2 = located_entity(2, entity_t::T_DIRT, 4, 5);
        let ent3 = located_entity(3, entity_t::T_DIRT, 1, 1);
        gd.entities.insert(ent1.id, ent1.clone());
        gd.entities.insert(ent2.id, ent2.clone());
        gd.entities.insert(ent3.id, ent3.clone());

        gd.update_location_entities_cache();

        if let Some(bucket) = gd.location_entities_cache.get(&ent1.location) {
            assert!(bucket.contains(&ent1), "ent1 not found in bucket");
            assert!(bucket.contains(&ent2), "ent2 not found in bucket");
            assert!(!bucket.contains(&ent3), "ent3 found in bucket");
            // make sure that items are sorted in rendering order
            assert_eq!(&bucket[0], &ent2)
        } else {
            panic!("Expected bucket in location entity cache not found");
        }
        if let Some(bucket) = gd.location_entities_cache.get(&ent3.location) {
            assert!(!bucket.contains(&ent1), "ent1 found in bucket");
            assert!(!bucket.contains(&ent2), "ent2 found in bucket");
            assert!(bucket.contains(&ent3), "ent3 not found in bucket");
        } else {
            panic!("Expected bucket in location entity cache not found");
        }
        assert!(gd.location_entities_cache.get(&Location {x: 0, y: 1}).is_none(),
                "Found unexpected location cache entry");
    }
}
