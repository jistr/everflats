use crate::diagnostic;
use common::types::*;
use common::protocol::*;
use crossbeam_channel::{bounded, Sender, Receiver, TryRecvError};
use log::*;
use wasm_bindgen::{closure::Closure, JsCast, JsValue};
use web_sys::{ErrorEvent, MessageEvent, WebSocket};

const INCOMING_BUFFER_SIZE: usize = 1024;

pub struct Connection {
    socket: Option<WebSocket>,
    incoming: Receiver<Msg>,

    incoming_sender: Sender<Msg>,
}

impl Connection {
    pub fn new() -> Self {
        let (inc_send, inc_recv) = bounded::<Msg>(INCOMING_BUFFER_SIZE);

        Self {
            socket: None,
            incoming: inc_recv,

            incoming_sender: inc_send,
        }
    }

    pub fn connect(&mut self, srv_uri: &str) -> Result<()> {
        if let Some(socket) = self.socket.take() {
            socket.close()?;
        }

        let socket = WebSocket::new(srv_uri)?;
        socket.set_binary_type(web_sys::BinaryType::Arraybuffer);

        let onmessage_sender = self.incoming_sender.clone();
        let onmessage_callback = Closure::wrap(Box::new(move |e: MessageEvent| {
            if let Ok(abuf) = e.data().dyn_into::<js_sys::ArrayBuffer>() {
                let array = js_sys::Uint8Array::new(&abuf);
                diagnostic::received_bytes(array.byte_length() as usize);
                // Is to_vec() the most efficient way here?
                match Msg::from_bincode(&array.to_vec()) {
                    Ok(msg) => {
                        trace!("Received message: {:?}", msg);
                        if let Err(e) = onmessage_sender.try_send(msg) {
                            error!("Error queuing a message from websocket: {}", e);
                        }
                    },
                    Err(e) => error!("Error decoding message: {}", e),
                }
            } else {
                error!("Received unexpected message from server: {:?}", e.data());
            }
        }) as Box<dyn FnMut(MessageEvent)>);
        socket.set_onmessage(Some(onmessage_callback.as_ref().unchecked_ref()));
        onmessage_callback.forget();

        let onerror_callback = Closure::wrap(Box::new(move |e: ErrorEvent| {
            error!("Websocket error: {:?}", e);
        }) as Box<dyn FnMut(ErrorEvent)>);
        socket.set_onerror(Some(onerror_callback.as_ref().unchecked_ref()));
        onerror_callback.forget();

        let onopen_callback = Closure::wrap(Box::new(move |_| {
            info!("Websocket connected");
        }) as Box<dyn FnMut(JsValue)>);
        socket.set_onopen(Some(onopen_callback.as_ref().unchecked_ref()));
        onopen_callback.forget();

        let onclose_callback = Closure::wrap(Box::new(move |_| {
            info!("Websocket disconnected");
        }) as Box<dyn FnMut(JsValue)>);
        socket.set_onclose(Some(onclose_callback.as_ref().unchecked_ref()));
        onclose_callback.forget();

        self.socket = Some(socket);

        Ok(())
    }

    pub fn receive(&mut self) -> Result<Option<Msg>> {
        match self.incoming.try_recv() {
            Ok(msg) => Ok(Some(msg)),
            Err(e) => match e {
                TryRecvError::Empty => Ok(None),
                _ => Err(e.into()),
            },
        }
    }

    pub fn send(&mut self, msg: Msg) -> Result<()> {
        match &self.socket {
            Some(socket) => Ok(socket.send_with_u8_array(&msg.to_bincode()?)?),
            None => Err(Error::network("No websocket open")),
        }
    }
}
