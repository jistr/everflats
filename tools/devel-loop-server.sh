#!/bin/bash

function run-server() {
    CARGO_BUILD_FLAGS='' make build-server
    export RUST_LOG=debug
    export RUST_BACKTRACE=1
    ./target/debug/everflats_server &
}

function kill-server() {
    kill %%
    wait
}

trap "{ kill-server; exit 1; }" SIGINT SIGTERM

while true; do
    run-server
    read
    kill-server
done
