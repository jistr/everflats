#!/bin/bash

pushd wui
npm run start &
popd

while true; do
    TARGET_SUBDIR=debug CARGO_BUILD_FLAGS='' NPX_WEBPACK_MODE=development make build-wui \
        && touch wui/static/index.html
    read
done
