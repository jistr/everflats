extern crate proc_macro;
#[macro_use]
extern crate quote;
extern crate syn;

use proc_macro::TokenStream;
use std::iter::FromIterator;
use quote::ToTokens;
use syn::Expr;

#[proc_macro]
pub fn const_list(input: TokenStream) -> TokenStream {
    let expr: Expr = syn::parse(input).expect("Failed to parse input");
    let mut tuple_iter = match expr {
        Expr::Tuple(tuple) => tuple.elems.into_iter(),
        _ => panic!("Expected a tuple as macro parameter"),
    };

    let type_expr = tuple_iter.next().expect("Parameter must be tuple with 2 items");
    let items_expr = tuple_iter.next().expect("Parameter must be tuple with 2 items");

    let type_ = match type_expr {
        Expr::Path(path) => path,
        _ => panic!("Expected first parameter to be a path/type"),
    };

    let items_iter = match items_expr {
        Expr::Array(array) => array.elems.into_iter(),
        _ => panic!("Expected second parameter to be an array: {:?}", items_expr),
    };

    let items: Vec<(Expr, Expr)> = items_iter.map(|tuple_expr| {
        let mut tuple_iter = match tuple_expr {
            Expr::Tuple(tuple) => tuple.elems.into_iter(),
            _ => panic!("Expected array item to be a tuple"),
        };
        let name = tuple_iter.next().expect("Array item must be tuple with 2 items");
        let value = tuple_iter.next().expect("Array item must be tuple with 2 items");
        (name, value)
    }).collect();

    let mut outputs: Vec<TokenStream> = vec![];

    // Generate the constants
    for (name, value) in items.iter() {
        outputs.push(quote!{
            pub const #name: #type_ = #value;
        }.into());
    }

    // Generate name lookup function
    let mut match_arms: Vec<Box<dyn ToTokens>> = vec![];
    for (name, _) in items.iter() {
        let name_lower: String = match name {
            Expr::Path(exprpath) => {
                let segments = &exprpath.path.segments;
                segments.first().expect("No path segments in const name")
                    .ident.clone().into_token_stream().to_string().to_lowercase()
            },
            _ => panic!("Unexpected formatting of constant name"),
        };
        match_arms.push(Box::new(quote!{#name => #name_lower,}) as Box<dyn ToTokens>);
    }
    outputs.push(quote!{
        pub fn name(et: EntityT) -> &'static str {
            match et {
                #(#match_arms)*
                _ => panic!("Tried to look up name of unknown entity type."),
            }
        }
    }.into());

    TokenStream::from_iter(outputs)
}
