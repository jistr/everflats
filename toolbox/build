#!/bin/bash
set -euxo pipefail

BUILDAH='buildah'
BUILDAH_CONTAINER=everflats-toolbox-build
FROM=registry.fedoraproject.org/fedora:32
IMAGE_NAME=everflats-toolbox

function build_steps() {
    BRC="$BUILDAH run $BUILDAH_CONTAINER --"
    CONTAINER_ENV_PATH='/usr/share/Modules/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/.local/bin:/root/.cargo/bin'

    $BRC dnf -y update --refresh
    $BRC dnf -y install \
         gcc gcc-c++ git less make passwd sudo tmux \
         npm openssl-devel \
         alsa-lib-devel cmake freetype-devel expat-devel libxcb-devel
    $BRC dnf clean all
    $BRC bash -c 'echo "export PS1='\''[\u@\h \W]\$ '\''" >> /root/.bashrc'
    $BRC bash -c "echo 'export PATH=\"$CONTAINER_ENV_PATH\"' >> /root/.bashrc"
    $BRC curl -o /root/.rustup-pipeshell https://sh.rustup.rs
    $BRC bash -c 'cat /root/.rustup-pipeshell | sh -s -- -y'
    $BRC bash -c 'source /root/.bashrc && rustup target add wasm32-unknown-unknown'
    $BUILDAH config --env PATH="$CONTAINER_ENV_PATH" $BUILDAH_CONTAINER
    $BUILDAH config -v /root/project $BUILDAH_CONTAINER
    $BUILDAH config --cmd "/bin/sleep infinity" $BUILDAH_CONTAINER
    $BUILDAH config --workingdir /root/project $BUILDAH_CONTAINER

    $BUILDAH commit $BUILDAH_CONTAINER $IMAGE_NAME
}

if [ "${NO_RM:-}" != "1" ]; then
    $BUILDAH rm $BUILDAH_CONTAINER || true
fi
if ! buildah inspect $BUILDAH_CONTAINER &> /dev/null; then
    $BUILDAH from --name=$BUILDAH_CONTAINER $FROM
fi

build_steps

if [ "${NO_RM:-}" != "1" ]; then
    $BUILDAH rm $BUILDAH_CONTAINER
fi
